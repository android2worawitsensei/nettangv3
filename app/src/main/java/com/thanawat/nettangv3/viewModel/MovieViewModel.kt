package com.thanawat.nettangv3.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.thanawat.nettangv3.model.Movie

class MovieViewModel(): ViewModel() {
    var check = 0
    val favoriteMovie = mutableListOf<Movie>()
    fun addFavoriteMovie(movie: Movie){
        favoriteMovie.add(movie)
        Log.d("favorite",favoriteMovie.toString())
    }

    fun removeFavoriteMovie(movie: Movie){
        favoriteMovie.remove(movie)
        Log.d("favorite",favoriteMovie.toString())
    }
    fun checkAddFavoriteMovie(movie: Movie){
        if(check==0){
            addFavoriteMovie(movie)
            check = 1
        }
        else {
            removeFavoriteMovie(movie)
            check = 0
        }
    }

}