package com.thanawat.nettangv3.model

import androidx.annotation.DrawableRes

data class Favorite(
    @DrawableRes
    val imageId: Int
)
