package com.thanawat.nettangv3.model

import androidx.annotation.DrawableRes

data class Movie(
    val id: Int,
    val name: String,
    val time: String,
    val description: String,
    @DrawableRes val imageId: Int,
)