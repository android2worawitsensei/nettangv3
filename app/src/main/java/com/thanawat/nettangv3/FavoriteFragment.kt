package com.thanawat.nettangv3

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.thanawat.nettangv3.adapter.AdapterFavoriteItem
import com.thanawat.nettangv3.adapter.AdapterHomeItem
import com.thanawat.nettangv3.data.MovieData
import com.thanawat.nettangv3.databinding.FragmentFavoriteBinding
import com.thanawat.nettangv3.databinding.FragmentHomeBinding
import com.thanawat.nettangv3.viewModel.MovieViewModel

class FavoriteFragment : Fragment() {
    private val movieViewModel: MovieViewModel by activityViewModels()
    private var _binding: FragmentFavoriteBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFavoriteBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val favorite = movieViewModel.favoriteMovie
        val recyclerViewAction = binding.favoriteRecyclerView
        recyclerViewAction.adapter = AdapterFavoriteItem(requireContext(), favorite) {
            Log.d("it",it.toString())
            movieViewModel.removeFavoriteMovie(it)
        }
        recyclerViewAction.layoutManager = LinearLayoutManager(requireContext())

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}