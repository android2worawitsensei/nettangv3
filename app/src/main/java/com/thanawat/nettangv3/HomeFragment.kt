package com.thanawat.nettangv3

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.thanawat.nettangv3.adapter.AdapterHomeItem
import com.thanawat.nettangv3.data.MovieData
import com.thanawat.nettangv3.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieAction = MovieData().getAction()
        val movieDrama = MovieData().getDrama()
        val movieAdventure = MovieData().getAdventure()
        val recyclerViewAction = binding.ActionRecyclerView
        recyclerViewAction.adapter = AdapterHomeItem(requireContext(), movieAction) {
            val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(itemId = it.id, typeMovie = 0)
            findNavController().navigate(action)
            Log.d("click","Click")
        }
        val recyclerViewAdventure = binding.AdventureRecyclerView
        recyclerViewAdventure.adapter = AdapterHomeItem(requireContext(), movieAdventure) {
            val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(itemId = it.id, typeMovie = 1)
            findNavController().navigate(action)
            Log.d("click","Click")

        }
        val recyclerViewDrama = binding.DramaRecyclerView
        recyclerViewDrama.adapter = AdapterHomeItem(requireContext(), movieDrama) {
            val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(itemId = it.id, typeMovie = 2)
            findNavController().navigate(action)
            Log.d("click","Click")
        }

        recyclerViewAction.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        recyclerViewDrama.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        recyclerViewAdventure.layoutManager = LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)

        binding.btnToFavorite.setOnClickListener{
            val action = HomeFragmentDirections.actionHomeFragmentToFavoriteFragment2()
            findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}