package com.thanawat.nettangv3.data

import com.thanawat.nettangv3.R
import com.thanawat.nettangv3.model.Favorite
import com.thanawat.nettangv3.model.Movie

class FavoriteData {
    fun getFavorite(): List<Favorite> {
        return listOf(
            Favorite(
                imageId = R.drawable.ic_baseline_favorite_24
            ),
            Favorite(
                imageId = R.drawable.ic_baseline_favorite_border_24
            ),
        )
    }
}