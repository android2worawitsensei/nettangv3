package com.thanawat.nettangv3.data

import com.thanawat.nettangv3.R
import com.thanawat.nettangv3.model.Movie

class MovieData {
    fun getAction(): List<Movie> {
        return listOf(
            Movie(
                id = 1,
                name = "BlackAdam",
                time = "2h 10m",
                description = "In the heart of an enchanted city, a commoner named Aladdin and his mischievous monkey, Abu, battle to save the free-spirited Princess Jasmine. Aladdin's life changes with one rub of a magic lamp as a fun-loving, shape-shifting Genie appears and grants him three wishes. Through his adventures",
                imageId = R.drawable.blackadam
            ),
            Movie(
                id = 2,
                name = "AVENGER END GAME",
                time = "1h 59m",
                description = "After half of all life is snapped away by Thanos, the Avengers are left scattered and divided. Now with a way to reverse the damage, the Avengers and their allies must assemble once more and learn to put differences aside in order to work together and set things right.",
                imageId = R.drawable.avenger2
            ),
            Movie(
                id = 3,
                name = "Thor Love and Thunder",
                time = "2h 30m",
                description = "\"Thor: Love and Thunder\" finds Thor (Chris Hemsworth) on a journey unlike anything he's ever faced -- a quest for inner peace. But his retirement is interrupted by a galactic killer known as Gorr the God Butcher (Christian Bale), who seeks the extinction of the gods.\"",
                imageId = R.drawable.thor
            ),
            Movie(
                id = 4,
                name = "Morbius",
                time = "2h 15m",
                description = "\"Morbius,\" directed by Daniel Espinosa, is the latest addition to the list of Marvel characters that have made it on screen. Jared Leto plays the genius scientist, Michael Morbius, who had rejected a Nobel Prize because he believed that he didn’t deserve an award for a job half done.",
                imageId = R.drawable.morbius
            )

        )
    }
    fun getAdventure(): List<Movie> {
        return listOf(
            Movie(
                id = 1,
                name = "Jurassic",
                time = "2h 59m",
                description = "In the heart of an enchanted city, a commoner named Aladdin and his mischievous monkey, Abu, battle to save the free-spirited Princess Jasmine. Aladdin's life changes with one rub of a magic lamp as a fun-loving, shape-shifting Genie appears and grants him three wishes. Through his adventures",
                imageId = R.drawable.jurassic
            ),
            Movie(
                id = 2,
                name = "Doctor Strange 2",
                time = "2h 30m",
                description = "What Is Doctor Strange 2 About? Marvel's latest MCU installment follows the journey of Doctor Steven Strange as he traverses the multiverse protecting his newest powered compatriot, America Chavez, from Wanda Maximoff",
                imageId = R.drawable.docter2
            ),
            Movie(
                id = 3,
                name = "Adam Project",
                time = "2h 30m",
                description = "After accidentally crash-landing in 2022, time-traveling fighter pilot Adam Reed teams up with his 12-year-old self for a mission to save the future. After accidentally crash-landing in 2022, time-traveling fighter pilot Adam Reed teams up with his 12-year-old self for a mission to save the future.",
                imageId = R.drawable.adamproject
            ),
            Movie(
                id = 4,
                name = "Spider Man No Way Home",
                time = "2h 30m",
                description = "When a spell goes wrong, dangerous foes from other worlds start to appear, forcing Peter to discover what it truly means to be Spider-Man. Peter Parker's secret identity is revealed to the entire world. Desperate for help, Peter turns to Doctor Strange to make the world forget that he is Spider-Man.",
                imageId = R.drawable.spiderman
            )
        )
    }
    fun getDrama(): List<Movie> {
        return listOf(
            Movie(
                id = 1,
                name = "20th Century Girl (2022)",
                time = "2h 29m",
                description = "In 1999, a teen girl keeps close tabs on a boy in school on behalf of her deeply smitten best friend – then she gets swept up in a love story of her own. Watch all you want.",
                imageId = R.drawable._0th_century
            ),
            Movie(
                id = 2,
                name = "TOP GUN",
                time = "2h 30m",
                description = "The Top Gun Naval Fighter Weapons School is where the best of the best train to refine their elite flying skills. When hotshot fighter pilot Maverick (Tom Cruise) is sent to the school, his reckless attitude and cocky demeanor put him at odds with the other pilots, especially the cool and collected Iceman (Val Kilmer).",
                imageId = R.drawable.topgun
            ),
            Movie(
                id = 3,
                name = "Mulan (2020)",
                time = "2h 19m",
                description = "A young Chinese maiden disguises herself as a male warrior in order to save her father. A young Chinese maiden disguises herself as a male warrior in order to save her father. A young Chinese maiden disguises herself as a male warrior in order to save her father.",
                imageId = R.drawable.mulan
            ),
            Movie(
                id = 4,
                name = "Interceptor",
                time = "1h 59m",
                description = "One Army captain must use her years of tactical training and military expertise when a simultaneous coordinated attack threatens the remote missile interceptor station she is in command of.",
                imageId = R.drawable.intercepter
            )
        )
    }
}