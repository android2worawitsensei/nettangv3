package com.thanawat.nettangv3.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thanawat.nettangv3.R
import com.thanawat.nettangv3.model.Movie

class AdapterFavoriteItem (
    private val context: Context,
    private val dataset: MutableList<Movie>,
    private val onItemClicked: (Movie) -> Unit
) : RecyclerView.Adapter<AdapterFavoriteItem.ItemViewHolder>() {
    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageFavorite: ImageView = view.findViewById(R.id.movieImage)
        val nameFavorite: TextView = view.findViewById(R.id.AdapterName)
        val timeFavorite: TextView = view.findViewById(R.id.AdapterTime)
        val removeFavorite: ImageView = view.findViewById(R.id.remove)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        // create a new view
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_favorite, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.imageFavorite.setImageResource(item.imageId)
        holder.nameFavorite.text = item.name
        holder.timeFavorite.text = item.time
        holder.removeFavorite.setOnClickListener {
            onItemClicked(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount() = dataset.size
}