package com.thanawat.nettangv3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.card.MaterialCardView
import com.thanawat.nettangv3.R
import com.thanawat.nettangv3.model.Movie

class AdapterHomeItem(
    private val context: Context,
    private val dataset: List<Movie>,
    private val onItemClicked: (Movie) -> Unit
) : RecyclerView.Adapter<AdapterHomeItem.ItemViewHolder>() {
    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.findViewById(R.id.poster_image_btn)
        val itemMovie: ImageButton = view.findViewById(R.id.poster_image_btn)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        // create a new view
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.adapter_home, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataset[position]
        holder.imageView.setImageResource(item.imageId)
        holder.itemMovie.setOnClickListener {
            onItemClicked(item)
        }
    }

    override fun getItemCount() = dataset.size
}
