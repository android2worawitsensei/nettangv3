package com.thanawat.nettangv3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.thanawat.nettangv3.data.FavoriteData
import com.thanawat.nettangv3.data.MovieData
import com.thanawat.nettangv3.databinding.FragmentDetailBinding
import com.thanawat.nettangv3.databinding.FragmentFavoriteBinding
import com.thanawat.nettangv3.model.Favorite
import com.thanawat.nettangv3.model.Movie
import com.thanawat.nettangv3.viewModel.MovieViewModel


class DetailFragment : Fragment() {
    private val navigationArgs: DetailFragmentArgs by navArgs()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private var itemId = 0
    private var typeMovie = 0
    val movieAction = MovieData().getAction()
    val movieAdventure = MovieData().getAdventure()
    val movieDrama = MovieData().getDrama()
    val favoriteSign = FavoriteData().getFavorite()
    private lateinit var movie: Movie
    private lateinit var favorite: Favorite
    private val movieViewModel: MovieViewModel by activityViewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        itemId = navigationArgs.itemId
        typeMovie = navigationArgs.typeMovie
        //Action
        if (typeMovie == 0) {
            movieAction.forEach {
                if (it.id == itemId) {
                    movie = it
                }
            }
            setTextOnDetailFragment()
        }
        //Adventure
        else if (typeMovie == 1) {
            movieAdventure.forEach {
                if (it.id == itemId) {
                    movie = it
                }
            }
            setTextOnDetailFragment()
        }
        //Drama
        else if (typeMovie == 2) {
            movieDrama.forEach {
                if (it.id == itemId) {
                    movie = it
                }
            }
            setTextOnDetailFragment()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setTextOnDetailFragment() {
        binding.apply {
            detailImage.setImageResource(movie.imageId)
            detailName.text = movie.name
            detailTime.text = movie.time
            Detail.text = movie.description
            addFavoriteItem()

        }

    }

    private fun addFavoriteItem(){
        binding.addFavorite.setOnClickListener {
            movieViewModel.checkAddFavoriteMovie(Movie(
                movie.id,
                movie.name,
                movie.time,
                movie.description,
                movie.imageId
            ))
            if(movieViewModel.check == 0){
                binding.addFavorite.setImageResource(favoriteSign[1].imageId)
            }else{
                binding.addFavorite.setImageResource(favoriteSign[0].imageId)
            }
        }
    }
}